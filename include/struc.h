/* structure for the properties of the atoms */
struct Atom
{
   double  rx, ry, rz;  /* position */
   double  px, py, pz;  /* momentum */
   double  fx, fy, fz;  /* force */
   int     g;           /* ghost index */
};

