/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

/* integration step for the interpolation */
void integrateStep_shadow(struct Atom atoms[]);

/* calculate a shadow Hamiltonian */
double shadow(double U, double K, double extra_term, struct Atom atoms[]);

/* Metropolis test with shadow Hamiltonians for GSHMC */
int metropolis_shadow(struct Atom atoms[], double oldsH, double sH,
                      int count, int m, int flag_move, double Z, double V);

/* Metropolis test with shadow Hamiltonians for the momenta GHMC */
int metropolis_momenta(double oldsH, double sH, int count);

/* momentum update for GSHMC */
void momentum_update_shadow(struct Atom atoms[], int count);

/* compute the weights */
double weight(double sH, double U, double k);
