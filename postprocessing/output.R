args<-commandArgs(TRUE)
if(length(args) != 4) stop("Four arguments are required\n")

library(lattice)
library(coda)
source("ESScoda.R")
source("ESScoda_w2.R")

Burn_in = as.numeric(args[1])
N = as.numeric(args[2])
niter = N - Burn_in

filename <- as.character(args[3])
if(file.exists(filename) != TRUE) stop("The file does not exist\n")

dist <- read.table(filename, header = FALSE, sep = " ")
dist <- dist[,2]
nsamples <- length(dist)
if(N + Burn_in > nsamples) stop("There are not enough samples in '",filename,"'\n")

gshmc <- as.logical(args[4])

if(gshmc == TRUE)
{
   linw <- scan("weights", "") # read lines
   weight <- matrix(as.numeric(linw),nrow = length(linw))

   weight <- weight[(Burn_in+1):N]
   sum_w <- sum(weight)
   
   # normalize weights
   weight_norm <- length(weight)*weight/sum_w
}

# trajectories / posterior samples
traj <- dist[(Burn_in+1):N]

if(gshmc == TRUE)
{
   trajAVG <- traj*weight_norm
   # write to the summary file
   out <- print(ESScoda_w2(traj,weight))
} else {
   trajAVG <- traj
   # write to the summary file
   out <- print(ESScoda(traj))
}


ESS <- out[,1]
MCSE <- out[,2]

IACT <- (niter/ESS)

essfile <- paste("ESS")
essfile <- file(essfile, "w+")
cat(ESS, '\n', file = essfile)
cat(IACT, '\n', file = essfile)
close(essfile)

mcsefile <- paste("MCSE")
mcsefile <- file(mcsefile, "w+")
cat(MCSE, '\n', file = mcsefile)
close(mcsefile)

# averages
avgfile <- paste("Averages")
avgfile <- file(avgfile, "w+")
avg <- mean(trajAVG)
cat(avg, '\n', file = avgfile)
close(avgfile)
