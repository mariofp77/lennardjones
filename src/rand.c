/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"

/* function for gaussian random variables */
double gaussian()
{
   static int    have = 0;
   static double x2;
   double fac, y1, y2, x1;
 
   if ( have == 1 )  /* already one available ? */
   {
      have = 0;
      return x2;
   } 
   else 
   {
      /* generate a pair of random variables */
      y1 = gsl_rng_uniform(g_rng);
      y2 = gsl_rng_uniform(g_rng);
      fac = sqrt(-2*log(y1));
      have = 1;
      x1 = fac*sin(2*M_PI*y2); /* x1 and x2 are now gaussian */
      x2 = fac*cos(2*M_PI*y2); /* so store one */
      return x1;               /* and return the other. */
   }
}

/* function for uniform random variables */
int uniform(int n)
{
   double x;

   x = gsl_rng_uniform(g_rng);
   x = round(x*n);

   return x;
}
