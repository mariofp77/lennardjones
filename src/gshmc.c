/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"
#include "rand.h"
#include "force.h"
#include "hmc.h"

/* integration step for the interpolation */
void integrateStep_shadow(struct Atom atoms[])
{
   int i;
   double store_U, store_vir;

   /* half-force step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = atoms[i].px + 0.5*dt*atoms[i].fx;
      atoms[i].py = atoms[i].py + 0.5*dt*atoms[i].fy;
      atoms[i].pz = atoms[i].pz + 0.5*dt*atoms[i].fz;
   }

   /* full free motion step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].rx = atoms[i].rx + dt*atoms[i].px;
      atoms[i].ry = atoms[i].ry + dt*atoms[i].py;
      atoms[i].rz = atoms[i].rz + dt*atoms[i].pz;
   }

   /* positions were changed, so recompute the forces.
      Maintain the potential energy and the virial */
   store_U = U;
   store_vir = vir;
   computeForces(atoms);
   U = store_U;
   vir = store_vir;

   /* final force half-step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = atoms[i].px + 0.5*dt*atoms[i].fx;
      atoms[i].py = atoms[i].py + 0.5*dt*atoms[i].fy;
      atoms[i].pz = atoms[i].pz + 0.5*dt*atoms[i].fz;
   }
}

/* calculate a shadow Hamiltonian */
double shadow(double U, double K, double extra_term, struct Atom atoms[])
{
   double shadow;
   struct Atom atoms_local[M];
   struct Atom atoms_for1[M], atoms_for2[M], atoms_back1[M], atoms_back2[M];
   double dvx2[M], dvy2[M], dvz2[M], dvx1[M], dvy1[M], dvz1[M];
   double drx3[M], dry3[M], drz3[M], drx2[M], dry2[M], drz2[M], drx1[M], dry1[M], drz1[M];
   double D02 = 0.0, D11 = 0.0;
   double D13 = 0.0, D22 = 0.0;
   int i;

   /* first sum the terms that are not defined with derivatives */
   shadow = U + K + extra_term;

   /* store the structure at which the shadow Hamiltonian
      is evaluated */
   store_atoms(atoms, atoms_local);
   /* FORWARD */
   /* integrate one step and store the structure */ 
   integrateStep_shadow(atoms);
   store_atoms(atoms, atoms_for1);
   /* integrate one step and store the structure */ 
   integrateStep_shadow(atoms);
   store_atoms(atoms, atoms_for2);
   /* FORWARD */

   /* BACKWARD */
   /* go back to the structure at which the shadow
      Hamiltonian is evaluated */
   store_atoms(atoms_local, atoms);
   /* flip the momenta to integrate backwards */
   momentum_flip(atoms);
   /* integrate one step and store the structure */ 
   integrateStep_shadow(atoms);
   store_atoms(atoms, atoms_back1);
   /* integrate one step and store the structure */ 
   integrateStep_shadow(atoms);
   store_atoms(atoms, atoms_back2);
   /* BACKWARD */

   /* recover the structure at which the shadow Hamiltonian
      is evaluated */
   store_atoms(atoms_local, atoms);

   for ( i = 0; i < M; i = i + 1 ) 
   {
      /* calculate the second derivative of the velocities */
      //dvx2[i] = (atoms_back1[i].px - 2.0*atoms[i].px + atoms_for1[i].px)*atoms[i].g;
      //dvy2[i] = (atoms_back1[i].py - 2.0*atoms[i].py + atoms_for1[i].py)*atoms[i].g;
      //dvz2[i] = (atoms_back1[i].pz - 2.0*atoms[i].pz + atoms_for1[i].pz)*atoms[i].g;
      
      /* calculate the first derivative of the velocities */
      //dvx1[i] = (atoms_for1[i].px - atoms_back1[i].px)*atoms[i].g/2.0;
      //dvy1[i] = (atoms_for1[i].py - atoms_back1[i].py)*atoms[i].g/2.0;
      //dvz1[i] = (atoms_for1[i].pz - atoms_back1[i].pz)*atoms[i].g/2.0;
      
      /* D02 = dvx2*atoms.px + dvy2*atoms.py + dvz2*atoms.pz */
      //D02 = D02 + dvx2[i]*atoms[i].px + dvy2[i]*atoms[i].py + dvz2[i]*atoms[i].pz;
      /* D11 = dvx1*dvx1 + dvy1*dvy1 + dvz1*dvz1 */
      //D11 = D11 + dvx1[i]*dvx1[i] + dvy1[i]*dvy1[i] + dvz1[i]*dvz1[i];

      /* calculate the third derivative of the positions */
      drx3[i] = (-atoms_back2[i].rx + 2.0*atoms_back1[i].rx - 2.0*atoms_for1[i].rx + atoms_for2[i].rx)*atoms[i].g/2.0;
      dry3[i] = (-atoms_back2[i].ry + 2.0*atoms_back1[i].ry - 2.0*atoms_for1[i].ry + atoms_for2[i].ry)*atoms[i].g/2.0;
      drz3[i] = (-atoms_back2[i].rz + 2.0*atoms_back1[i].rz - 2.0*atoms_for1[i].rz + atoms_for2[i].rz)*atoms[i].g/2.0;

      /* calculate the second derivative of the positions */
      drx2[i] = (atoms_back1[i].rx - 2.0*atoms[i].rx + atoms_for1[i].rx)*atoms[i].g;
      dry2[i] = (atoms_back1[i].ry - 2.0*atoms[i].ry + atoms_for1[i].ry)*atoms[i].g;
      drz2[i] = (atoms_back1[i].rz - 2.0*atoms[i].rz + atoms_for1[i].rz)*atoms[i].g;
      
      /* calculate the first derivative of the positions */
      drx1[i] = (atoms_for1[i].rx - atoms_back1[i].rx)*atoms[i].g/2.0;
      dry1[i] = (atoms_for1[i].ry - atoms_back1[i].ry)*atoms[i].g/2.0;
      drz1[i] = (atoms_for1[i].rz - atoms_back1[i].rz)*atoms[i].g/2.0;

      /* D03 = drx3*drx1 + dry3*dry1 + drz3*drz1 */
      D13 = D13 + drx3[i]*drx1[i] + dry3[i]*dry1[i] + drz3[i]*dry1[i];
      /* D22 = drx2*drx2 + dry2*dry2 + drz2*drz2 */
      D22 = D22 + drx2[i]*drx2[i] + dry2[i]*dry2[i] + drz2[i]*drz2[i];
   }

   //shadow = shadow - (D02 + D11/2.0)/12.0;
   shadow = shadow - (D13 + D22/2.0)/(12.0*dt*dt);

   return shadow;
}

/* Metropolis test with shadow Hamiltonians for GSHMC */
int metropolis_shadow(struct Atom atoms[], double oldsH, double sH,
                      int count, int m, int flag_move, double Z, double V)
{
    int result;
    double r, Pr, dH;

    r = gsl_rng_uniform(g_rng);
    /* difference in energy */
    dH = sH - oldsH;
    Pr = exp(-beta*dH);

    if (flag_move == 1) /* insertion */
    {
       if (Z*V*Pr/(N+1) > r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Difference in shadow Hamiltonian = %f\n",dH);
             printf("Metropolis: We accept %f with probability %f\n",Z*V*Pr/(N+1),r);
          }
          result = 1; /* the new state is accepted */
          N = N+1; /* increase the number of particles */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of INSERTION\n");
             printf("The particle inserted is %d\n",m);
             printf("Difference in shadow Hamiltonian = %f\n",dH);
             printf("Metropolis: We reject %f with probability %f\n",Z*V*Pr/(N+1),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 0; /* the particle is kept ghost */
       }
    }
    else if (flag_move == 0) /* deletion */
    {
       if (N*Pr/(Z*V) >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Difference in shadow Hamiltonian = %f\n",dH);
             printf("Metropolis: We accept %f with probability %f\n",N*Pr/(Z*V),r);
          }
          result = 1; /* the new state is accepted */
          N = N-1; /* one particle is removed */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of DELETION\n");
             printf("The particle deleted is %d\n",m);
             printf("Difference in shadow Hamiltonian = %f\n",dH);
             printf("Metropolis: We reject %f with probability %f\n",N*Pr/(Z*V),r);
          }
          result = 0; /* the new state is rejected */
          atoms[m].g = 1; /* the particle is kept real */
       }
    }
    else if (flag_move == 2) /* move */
    {
       if (Pr >= r)
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("Difference in shadow Hamiltonian = %f\n",dH);
             printf("Metropolis: We accept %f with probability %f\n",Pr,r);
          }
          result = 1; /* the new state is accepted */
       }
       else
       {
          if (count%screen == 0)
          {
             printf("Attemp of MOVE\n");
             printf("Difference in shadow Hamiltonian = %f\n",dH);
             printf("Metropolis: We reject %f with probability %f\n",Pr,r);
          }
          result = 0; /* the new state is rejected */
       }
    }

    return result;
}

/* Metropolis test with shadow Hamiltonians for the momenta GHMC */
int metropolis_momenta(double oldsH, double sH, int count)
{
    int result;
    double r, Pr, dH;

    r = gsl_rng_uniform(g_rng);
    /* difference in energy */
    dH = sH - oldsH;
    Pr = exp(-beta*dH);

    printf("PMMC\n");
    if (Pr >= r)
    {
       if (count%screen == 0)
       {
          printf("Difference in shadow Hamiltonian = %f\n",dH);
          printf("Metropolis: We accept %f with probability %f\n",Pr,r);
       }
       result = 1; /* the new state is accepted */
    }
    else
    {
       if (count%screen == 0)
       {
          printf("Difference in shadow Hamiltonian = %f\n",dH);
          printf("Metropolis: We reject %f with probability %f\n",Pr,r);
       }
       result = 0; /* the new state is rejected */
    }

    return result;
}

/* momentum update for GSHMC */
void momentum_update_shadow(struct Atom atoms[], int count)
{
   double u[M][3], u_prime[M][3];
   double K_u = 0.0, K_uprime = 0.0;
   double oldK;
   struct Atom atoms_local[M];
   double sH_before, sH_after;
   int result;
   int i;

   /* store old kinetic energy and velocities */
   oldK = K;
   store_atoms(atoms, atoms_local);
   K = 0;

   for ( i = 0; i < M; i = i + 1 )
   {
      u[i][0] = noise();
      u_prime[i][0] = -sin(phi)*atoms[i].px + cos(phi)*u[i][0];
      atoms[i].px = cos(phi)*atoms[i].px + sin(phi)*u[i][0];
      u[i][1] = noise();
      u_prime[i][1] = -sin(phi)*atoms[i].py + cos(phi)*u[i][1];
      atoms[i].py = cos(phi)*atoms[i].py + sin(phi)*u[i][1];
      u[i][2] = noise();
      u_prime[i][2] = -sin(phi)*atoms[i].pz + cos(phi)*u[i][2];
      atoms[i].pz = cos(phi)*atoms[i].pz + sin(phi)*u[i][2];
      /* Kinetic energy for the updated momenta */
      K = K + atoms[i].g*(atoms[i].px*atoms[i].px
                        + atoms[i].py*atoms[i].py
                        + atoms[i].pz*atoms[i].pz);
      /* Kinetic energy for the noise u */
      K_u = K_u + atoms[i].g*(u[i][0]*u[i][0]
                            + u[i][1]*u[i][1]
                            + u[i][2]*u[i][2]);
      /* Kinetic energy for the updated noise u */
      K_uprime = K_uprime + atoms[i].g*(u_prime[i][0]*u_prime[i][0]
                                      + u_prime[i][1]*u_prime[i][1]
                                      + u_prime[i][2]*u_prime[i][2]);
   }

   /* compute kinetic energies */
   K = K/2;
   K_u = K_u/2;
   K_uprime = K_uprime/2;

   /* compute shadow Hamiltonian before momentum update */
   sH_before = shadow(U, oldK, K_u, atoms);
   /* compute shadow Hamiltonian before momentum update */
   sH_after = shadow(U, K, K_uprime, atoms);

   /* Metropolis test */
   result = metropolis_momenta(sH_before, sH_after, count);

   /* ACCEPT */
   if (result == 1)
   {
      printf("ACCEPTED\n");
      temp = 2*K/(3*N);
   }
   /* REJECT */
   else
   {
      printf("REJECTED\n");
      store_atoms(atoms_local, atoms);
      K = oldK;
      temp = 2*K/(3*N);
   }

   /* report results */
   if (count%screen == 0)
      printf("%f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n\n",
             count*dt, N, H/N, U/N, U, K/N, K, temp);
}

/* compute the weights */
double weight(double sH, double U, double k)
{
   double w;

   w = exp(-beta*((U + K) - sH));

   return w;
}
