/* 
 *  Authors: Ant\'onio M. Baptista,  Mario Fern\'andez-Pend\'as
 *
 *  Input (from reduced units): 
 *   number of particles 
 *   density of the system
 *   initial temperature (standard deviation of the velocities)
 *   number of steps
 *   time step 
 *   random number generator seed
 *   equilibration time 
 *
 *  Output: lines containing
 *   time, energy H, pot. en. U, kin. en. K, temperature T, fluctuations
 *
 *  Notes:
 *  - To compile: gcc lj.c -o ljexample -lm -O3 -ffast-math -w
 *  - To run: ljexample
 *        or: ljexample < input.ini > output.dat
 *    where input parameters are listed in the file "input.ini", 
 *    and output is redirected to the file "output.dat".
 *
 *  - Appropriate values for the equilibrium time are best found by
 *    doing a short run and seeing when the potential energy has reach a
 *    stationary value.
 *
 *  - All reported energies values are divided by the number of particles N.
 *
 *  - Fluctuations are the root mean square of H-<H>, with <H> the mean H.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "struc.h"
#include "global.h"
#include "pbc.h"
#include "sg.h"
#include "lj.h"
#include "ghost.h"
#include "hmc.h"
#include "mc.h"
#include "rand.h"
#include "force.h"
#include "gshmc.h"

/* function to calculate greatest common divisor */
int gcd(int a, int b)
{
    int t;
    while (b != 0)
    {
        t = a%b;
        a = b;
        b = t;
    }
    return a;
}

/* function to set up cubic lattice */
double latticex, latticey, latticez;
void makeLatticePosition(double a)
{
   static int i = 0;
   static int j = 0;
   static int k = 0;
   /* L length of the box. In this way there are no particles in the edge of the box */
   latticex = i*a - 0.5*(L-1.0);
   latticey = j*a - 0.5*(L-1.0);
   latticez = k*a - 0.5*(L-1.0);
   i = i + 1;
   if ( i*a > L - 1.0e-6 )   /* next point outside box? */
   {
      i = 0;                 /* then put back */
      j = j + 1;             /* and move to next y level */

      if ( j*a > L - 1.0e-6 )  /* outside box? */
      {
         j = 0;                /* then put back */
         k = k + 1;            /* and move to next z level */

         if ( k*a > L - 1.0e-6 )  /* outside box? */
         {
            i = 0;                /* then back to the start */
            j = 0;
            k = 0;
         }
      }
   }
}

/* function to initialize the system */
void initialize(struct Atom atoms[])
{
   double  scale, a;
   int     i,j;

   /* generate positions */
   a = L/(int)(cbrt(N)+0.99999999999); /* lattice distance */
   //a = L/(int)(cbrt(M)); /* lattice distance */

   for ( i = 0; i < N; i = i + 1 ) 
   {
      makeLatticePosition(a);
      atoms[i].rx = latticex;
      atoms[i].ry = latticey;
      atoms[i].rz = latticez;
   }
   for ( j = N; j < M; j = j +1 )
   {
      atoms[j].rx = (L-0.999999999)*drand48() - (L-0.999999999)/2;
      atoms[j].ry = (L-0.999999999)*drand48() - (L-0.999999999)/2;
      atoms[j].rz = (L-0.999999999)*drand48() - (L-0.999999999)/2;
   }

   /* generate momenta */
   scale = sqrt(T);
   K     = 0;

   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = scale*gaussian();
      atoms[i].py = scale*gaussian();
      atoms[i].pz = scale*gaussian();
      K = K
         + atoms[i].g*(atoms[i].px*atoms[i].px 
         + atoms[i].py*atoms[i].py 
         + atoms[i].pz*atoms[i].pz);
   }

   /* compute instantaneous kinetic temperature and energy */
   temp = K/(3*N); // 2/3 of the Kinetic energy
   K = K/2;

   /* compute force and potential energy U */
   computeForces(atoms);

   /* compute instantaneous pressure */
   pres = (N*temp + vir)/(L*L*L);

   /* compute total energy */
   H = U + K;

   /* report results */ 
   printf("# time   N   E/N       U/N   U      K/N         T   <[H-<H>]^2>\n");
   printf("%8.6f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n", 0., N, H/N, U/N, U, K/N, temp, 0.);
}

/* Verlet integration step */
void integrateStep(struct Atom atoms[])
{
   int i;

   /* half-force step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = atoms[i].px + 0.5*dt*atoms[i].fx;
      atoms[i].py = atoms[i].py + 0.5*dt*atoms[i].fy;
      atoms[i].pz = atoms[i].pz + 0.5*dt*atoms[i].fz;
   }

   /* full free motion step */
   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].rx = atoms[i].rx + dt*atoms[i].px;
      atoms[i].ry = atoms[i].ry + dt*atoms[i].py;
      atoms[i].rz = atoms[i].rz + dt*atoms[i].pz;
   }

   /* positions were changed, so recompute the forces */
   computeForces(atoms);

   /* final force half-step */
   K = 0;

   for ( i = 0; i < M; i = i + 1 ) 
   {
      atoms[i].px = atoms[i].px + 0.5*dt*atoms[i].fx;
      atoms[i].py = atoms[i].py + 0.5*dt*atoms[i].fy;
      atoms[i].pz = atoms[i].pz + 0.5*dt*atoms[i].fz;
      K = K 
         + atoms[i].px*atoms[i].px*atoms[i].g
         + atoms[i].py*atoms[i].py*atoms[i].g
         + atoms[i].pz*atoms[i].pz*atoms[i].g;
   }

   /* finish computing T, K, H, P */
   temp = K/(3*N); // 2/3 of the Kinetic energy
   K = K/2;
   H = U + K;
   pres = (N*temp + vir)/(L*L*L);
}

//AMB: Write system snapshot to PDB, labeling real/ghost as C/H atoms.
void write_to_PDB(struct Atom atoms[],int tindex)
{
  int i,j;

  fprintf(pdb,"MODEL %4d\n",tindex) ;
  for (j = 0; j < M; j++)
  {
    fprintf(pdb,"ATOM  %5d  X    LJ  %4d    %8.3f%8.3f%8.3f  1.00  0.00           %c\n",
	    j,j,atoms[j].rx,atoms[j].ry,atoms[j].rz,(atoms[j].g == 1 ? 'C' : 'H')) ;
  }
  fprintf(pdb,"TER\nENDMDL\n") ;
}

/* integration and measurement */
void run()
{
   struct  Atom atoms[M]; /* this is the main structure with the information of the particles */
   struct  Atom atoms_local[M]; /* this structure is used for storing the information of the particles during MD in HMC */
   struct  Atom atoms_sg[M]; /* this structure is used for the forces during the slow growth */
   int     count;          /* counts time steps */
   int     md_traj_count = 0; /* counts time steps in MD trajectory */
   int     numPoints = 0;  /* counts measurements */
   double  sumH      = 0;  /* total energy accumulated over steps */
   double  sumH2     = 0;  /* total energy squared accumulated */
   double  avgH, avgH2, fluctH;  /* average energy, square, fluctuations */
   int     naccept = 0;    /* the number of acceptances */
   int     iResult;        /* the result of each Monte Carlo test */
   int     part_indx = 0;  /* the index of the particle that changes */
   int     rand_indx = 0;  /* the random index for the particle that changes */
   int     flag_move;      /* the flag of the kind of move */
   double  oldU,U_sg;      /* store the energy before the MD trajectory */
   double  oldK;           /* store the energy before the MD trajectory */
   double  oldT;           /* store the temperature before the MD trajectory */
   double  oldP;           /* store the pressure before the MD trajectory */
   double  oldsH,sH;       /* store the shadow Hamiltonian before and after the MD trajectory */
   double  Z;              /* dimensionless Z */
   double  V;              /* the volume of the box */
   double  w = 1.0;        /* weights computed for GSHMC */
   FILE    *file = fopen("energies", "w");
   FILE    *file2 = fopen("metropolis", "w");
   FILE    *kinetic = fopen("kinetic", "w");
   FILE    *hamiltonians = fopen("hamiltonians", "w");
   FILE    *shadows = fopen("shadows", "w");
   FILE    *weights = fopen("weights", "w");
   const char *deletion="deleted", *insertion="inserted", *move="moved";

   /* define dimensionless Z and the voluem of the box */
   Z = exp(beta*mu);
   V = L*L*L;

   /* draw initial conditions */
   generateGhost(atoms);
   initialize(atoms);

   /* an index array where the real and ghost indexes are ordered */
   int *index,i;
   index = malloc(sizeof(int) * M);
   for (i = 0; i < M; i++) // initialization
      index[i] = i;

   /* write initial configuration to check how system starts */
   write_to_PDB(atoms,0);

   switch(met)
   {
      case 0: // HMC
         /* perform rest of time steps */
         for ( count = 1; count <= numSteps; count = count + 1 ) 
         {
            /* begining of each MD trajectory */
            if (md_traj_count == 0)
            {
               /* store the old potential and kinetic energies that will be used 
                  in metropolis() */
               oldU = U;
               oldK = K;
               /* store also the old temperature and pressure */
               oldT = temp;
               oldP = pres;

               /* store the positions before moving and removing/adding a particle */
               store_atoms(atoms,atoms_local);

               /* initialize the structure used in integrateStepGrow() before moving 
                  and removing/adding a particle */
               store_atoms(atoms,atoms_sg);

               /* decide the kind of move and the particle to be inserted.
                  The index g of the particle changes here */
               montecarloStep(atoms,index,&part_indx,&rand_indx,&flag_move);

               /* insertion/deletion */
               if (flag_move == 0 || flag_move == 1)
               {
                  /* compute forces in case of insertion/deletion. This is done
                     because the forces computed at the last integration step are not
                     useful anymore */
                  computeForces(atoms);
                  /* pretend that the particle was not inserted/deleted and compute
                     forces. This will be used in the slow growth */
                  U_sg = computeForcesGrow(atoms_sg); // U_sg is not used here
               }
               else
                  U_sg = 0.0;
            }
            
            /* perform integration step */
            if ( md_traj_count < il && (flag_move == 0 || flag_move == 1))
               integrateStepGrow(atoms,atoms_sg,part_indx,md_traj_count);
            else
               integrateStep(atoms);

            /* advance the counter of the MD trajectory */
            md_traj_count += 1;
            
            /* Metropolis test. End of each MD trajectory */
            if (count%iL == 0 & count != 0)
            {
               iResult = metropolis(atoms, oldU, oldK, U, K, count, part_indx, flag_move, Z, V);
               naccept = naccept + iResult;
               /* ACCEPT */
               if (iResult == 1) 
               {
                  if (count%screen == 0)
                     printf("ACCEPTED\n");
                  /* insertion/deletion */
                  if (flag_move == 0 || flag_move == 1)
                  {
                     /* print the file where the Metropolis results are stored */
                     fprintf(file2,"Time %f: The particle %d was %s\n",count*dt,part_indx,(flag_move == 0 ? deletion:insertion));
                     /* arrange the index array */
                     arrange_index(flag_move,index,rand_indx,part_indx);
                  }
                  /* move */
                  else
                  {
                     /* print the file where the Metropolis results are stored */
                     fprintf(file2,"Time %f: The system was moved\n",count*dt);
                  }
               }
               /* REJECT */
               else
               {
                  if (count%screen == 0)
                     printf("REJECTED\n");
                  store_atoms(atoms_local,atoms); // restore the positions
                  U = oldU; // restore the Potential energy
                  K = oldK; // restore the Kinetic energy
                  H = U + K; // restore the energy
                  temp = oldT; // restore the temperature
                  pres = oldP; // restore the pressure
               }

               /* PRINT RESULTS */
               /* accumulate energy and its square */
               sumH  = sumH + H;
               sumH2 = sumH2 + H*H;
               numPoints = numPoints + 1;
      
               /* determine averages and fluctuations */
               avgH   = sumH/numPoints;
               avgH2  = sumH2/numPoints;
               fluctH = sqrt(avgH2 - avgH*avgH); // standard deviation
      
               /* report results */
               if (count%screen == 0)
                  printf("%f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n\n",
                         count*dt, N, H/N, U/N, U, K/N, K, temp, fluctH/N);

               if (count%logfile == 0)
               {
                  /* write potential energy file */
                  fprintf(file,"%f %f %f %f\n",count*dt,U,U/N,pres);
                  /* write kinetic energy file */
                  fprintf(kinetic,"%f %f %f %f\n",count*dt,K,K/N,temp);
      
                  /* write post integration configuration */
                  write_to_PDB(atoms,count+1);
               }
               /* PRINT RESULTS */

               /* resample momenta completely. Kinetic energy and temperature
                  are also computed here */
               generate_momenta(atoms);

               /* set the counter of the MD trajectory back to zero */
               md_traj_count = 0;
            }
            /* Metropolis test. End of each MD trajectory */
         }
         break;
      case 1: // GHMC
         /* perform rest of time steps */
         for ( count = 1; count <= numSteps; count = count + 1 ) 
         {
            /* begining of each MD trajectory */
            if (md_traj_count == 0)
            {
               /* store the old potential and kinetic energies that will be used 
                  in metropolis() */
               oldU = U;
               oldK = K;
               /* store also the old temperature and pressure */
               oldT = temp;
               oldP = pres;

               /* store the positions before moving and removing/adding a particle */
               store_atoms(atoms,atoms_local);

               /* initialize the structure used in integrateStepGrow() before moving 
                  and removing/adding a particle */
               store_atoms(atoms,atoms_sg);

               /* decide the kind of move and the particle to be inserted.
                  The index g of the particle changes here */
               montecarloStep(atoms,index,&part_indx,&rand_indx,&flag_move);

               /* insertion/deletion */
               if (flag_move == 0 || flag_move == 1)
               {
                  /* compute forces in case of insertion/deletion. This is done
                     because the forces computed at the last integration step are not
                     useful anymore */
                  computeForces(atoms);
                  /* pretend that the particle was not inserted/deleted and compute
                     forces. This will be used in the slow growth */
                  U_sg = computeForcesGrow(atoms_sg); // U_sg is not used here
               }
               else
                  U_sg = 0.0;
            }
            
            /* perform integration step */
            if ( md_traj_count < il && (flag_move == 0 || flag_move == 1))
               integrateStepGrow(atoms,atoms_sg,part_indx,md_traj_count);
            else
               integrateStep(atoms);

            /* advance the counter of the MD trajectory */
            md_traj_count += 1;
            
            /* Metropolis test. End of each MD trajectory */
            if (count%iL == 0 & count != 0)
            {
               iResult = metropolis(atoms, oldU, oldK, U, K, count, part_indx, flag_move, Z, V);
               naccept = naccept + iResult;
               /* ACCEPT */
               if (iResult == 1) 
               {
                  if (count%screen == 0)
                     printf("ACCEPTED\n");
                  /* insertion/deletion */
                  if (flag_move == 0 || flag_move == 1)
                  {
                     /* print the file where the Metropolis results are stored */
                     fprintf(file2,"Time %f: The particle %d was %s\n",count*dt,part_indx,(flag_move == 0 ? deletion:insertion));
                     /* arrange the index array */
                     arrange_index(flag_move,index,rand_indx,part_indx);
                  }
                  /* move */
                  else
                  {
                     /* print the file where the Metropolis results are stored */
                     fprintf(file2,"Time %f: The system was moved\n",count*dt);
                  }
               }
               /* REJECT */
               else
               {
                  if (count%screen == 0)
                     printf("REJECTED\n");
                  store_atoms(atoms_local,atoms); // restore the positions
                  U = oldU; // restore the Potential energy
                  K = oldK; // restore the Kinetic energy
                  H = U + K; // restore the energy
                  temp = oldT; // restore the temperature
                  pres = oldP; // restore the pressure
                  momentum_flip(atoms); // flip the momenta
               }

               /* PRINT RESULTS */
               /* accumulate energy and its square */
               sumH  = sumH + H;
               sumH2 = sumH2 + H*H;
               numPoints = numPoints + 1;
      
               /* determine averages and fluctuations */
               avgH   = sumH/numPoints;
               avgH2  = sumH2/numPoints;
               fluctH = sqrt(avgH2 - avgH*avgH); // standard deviation
      
               /* report results */
               if (count%screen == 0)
                  printf("%f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n\n",
                         count*dt, N, H/N, U/N, U, K/N, K, temp, fluctH/N);

               if (count%logfile == 0)
               {
                  /* write potential energy file */
                  fprintf(file,"%f %f %f %f\n",count*dt,U,U/N,pres);
                  /* write kinetic energy file */
                  fprintf(kinetic,"%f %f %f %f\n",count*dt,K,K/N,temp);
      
                  /* write post integration configuration */
                  write_to_PDB(atoms,count+1);
               }
               /* PRINT RESULTS */

               /* update momenta. Kinetic energy and temperature
                  are also computed here */
               momentum_update(atoms);

               /* set the counter of the MD trajectory back to zero */
               md_traj_count = 0;
            }
            /* Metropolis test. End of each MD trajectory */
         }
         break;
      case 2: // GSHMC
         /* perform rest of time steps */
         for ( count = 1; count <= numSteps; count = count + 1 ) 
         {
            /* begining of each MD trajectory */
            if (md_traj_count == 0)
            {
               /* store the old potential and kinetic energies that will be used 
                  in metropolis() */
               oldU = U;
               oldK = K;
               /* calculate shadow Hamiltonian */
               oldsH = shadow(oldU, oldK, 0.0, atoms);
               /* store also the old temperature and pressure */
               oldT = temp;
               oldP = pres;

               /* store the positions before moving and removing/adding a particle */
               store_atoms(atoms,atoms_local);

               /* initialize the structure used in integrateStepGrow() before moving 
                  and removing/adding a particle */
               store_atoms(atoms,atoms_sg);

               /* decide the kind of move and the particle to be inserted.
                  The index g of the particle changes here */
               montecarloStep(atoms,index,&part_indx,&rand_indx,&flag_move);

               /* insertion/deletion */
               if (flag_move == 0 || flag_move == 1)
               {
                  /* compute forces in case of insertion/deletion. This is done
                     because the forces computed at the last integration step are not
                     useful anymore */
                  computeForces(atoms);
                  /* pretend that the particle was not inserted/deleted and compute
                     forces. This will be used in the slow growth */
                  U_sg = computeForcesGrow(atoms_sg); // U_sg is not used here
               }
               else
                  U_sg = 0.0;
            }
            
            /* perform integration step */
            if ( md_traj_count < il && (flag_move == 0 || flag_move == 1))
               integrateStepGrow(atoms,atoms_sg,part_indx,md_traj_count);
            else
               integrateStep(atoms);

            /* advance the counter of the MD trajectory */
            md_traj_count += 1;
            
            /* Metropolis test. End of each MD trajectory */
            if (count%iL == 0 & count != 0)
            {
               /* compute the shadow Hamiltonian at the end of the MD trajectory */
               sH = shadow(U, K, 0.0, atoms);
               /* do Metropolis test */
               iResult = metropolis_shadow(atoms, oldsH, sH, count, part_indx, flag_move, Z, V);
               naccept = naccept + iResult;
               /* ACCEPT */
               if (iResult == 1) 
               {
                  if (count%screen == 0)
                     printf("ACCEPTED\n");
                  /* insertion/deletion */
                  if (flag_move == 0 || flag_move == 1)
                  {
                     /* print the file where the Metropolis results are stored */
                     fprintf(file2,"Time %f: The particle %d was %s\n",count*dt,part_indx,(flag_move == 0 ? deletion:insertion));
                     /* arrange the index array */
                     arrange_index(flag_move,index,rand_indx,part_indx);
                  }
                  /* move */
                  else
                  {
                     /* print the file where the Metropolis results are stored */
                     fprintf(file2,"Time %f: The system was moved\n",count*dt);
                  }
                  /* compute weights */
                  w = weight(sH, U, K);
                  fprintf(weights,"%f\n",w);
               }
               /* REJECT */
               else
               {
                  if (count%screen == 0)
                     printf("REJECTED\n");
                  store_atoms(atoms_local,atoms); // restore the positions and velocities
                  U = oldU; // restore the Potential energy
                  K = oldK; // restore the Kinetic energy
                  H = U + K; // restore the energy
                  sH = oldsH; // restore the modified energy
                  temp = oldT; // restore the temperature
                  pres = oldP; // restore the pressure
                  momentum_flip(atoms); // flip the momenta
                  /* store the last computed weights */
                  fprintf(weights,"%f\n",w);
               }

               /* PRINT RESULTS */
               /* accumulate energy and its square */
               sumH  = sumH + H;
               sumH2 = sumH2 + H*H;
               numPoints = numPoints + 1;
      
               /* determine averages and fluctuations */
               avgH   = sumH/numPoints;
               avgH2  = sumH2/numPoints;
               fluctH = sqrt(avgH2 - avgH*avgH); // standard deviation
      
               /* report results */
               if (count%screen == 0)
                  printf("%f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n\n",
                         count*dt, N, H/N, U/N, U, K/N, K, temp, fluctH/N);

               if (count%logfile == 0)
               {
                  /* write potential energy file */
                  fprintf(file,"%f %f %f %f\n",count*dt,U,U/N,pres);
                  /* write kinetic energy file */
                  fprintf(kinetic,"%f %f %f %f\n",count*dt,K,K/N,temp);
                  /* write Hamiltonian file */
                  fprintf(hamiltonians,"%f %f\n",count*dt,U+K);
                  /* write shadow Hamiltonian file */
                  fprintf(shadows,"%f %f\n",count*dt,sH);
      
                  /* write post integration configuration */
                  write_to_PDB(atoms,count+1);
               }
               /* PRINT RESULTS */

               /* update momenta. Kinetic energy and temperature
                  are also computed here. The Metropolis test is also 
                  included */
               momentum_update_shadow(atoms, count);

               /* set the counter of the MD trajectory back to zero */
               md_traj_count = 0;
            }
            /* Metropolis test. End of each MD trajectory */
         }
         break;
      case 3: // Yao
         /* perform rest of time steps */
         for ( count = 1; count <= numSteps; count = count + 1 )
         {
            /* perform integration step */
            iResult = montecarloStep_yao(atoms,index,count,&part_indx,&flag_move,Z,V);
            naccept = naccept + iResult;
            if (iResult == 1) /* in case of accepted move print it */
            {
               fprintf(file2,"Step %d: The particle %d was %s\n",count,part_indx,(flag_move == 0 ? deletion:(flag_move == 1 ? insertion:move)));

               /* compute instantaneous pressure */
               pres = (N*T + vir)/(L*L*L);
            }
           
            if (count%logfile == 0)
            {
               /* write potential energy file */
               fprintf(file,"%f %f %f %f\n",count*dt,U,U/N,pres);
      
               /* write post integration configuration */
               write_to_PDB(atoms,count+1);
            }
      
            /* accumulate energy and its square */
            sumH  = sumH + H;
            sumH2 = sumH2 + H*H;
            numPoints = numPoints + 1;
      
            /* determine averages and fluctuations */
            avgH   = sumH/numPoints;
            avgH2  = sumH2/numPoints;
            fluctH = sqrt(avgH2 - avgH*avgH);
      
            /* report results */
            if (count%screen == 0)
               printf("%f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n",
                      count*dt, N, H/N, U/N, U, K/N, K, temp, fluctH/N);
         }
         break;
      case 4: // Rowley
         /* perform rest of time steps */
         for ( count = 1; count <= numSteps; count = count + 1 )
         {
            /* perform integration step */
            iResult = montecarloStep_rowley(atoms,count,&part_indx,&flag_move,Z,V);
            naccept = naccept + iResult;
            if (iResult == 1) /* in case of accepted move print it */
               fprintf(file2,"Step %d: The particle %d was %s\n",count,part_indx,(flag_move == 0 ? deletion:(flag_move == 1 ? insertion:move)));
           
            if (count%logfile == 0)
            {
               fprintf(file,"%f %f %f\n",count*dt,U,U/N);
      
               /* write post integration configuration */
               write_to_PDB(atoms,count+1);
            }
      
            /* accumulate energy and its square */
            sumH  = sumH + H;
            sumH2 = sumH2 + H*H;
            numPoints = numPoints + 1;
      
            /* determine averages and fluctuations */
            avgH   = sumH/numPoints;
            avgH2  = sumH2/numPoints;
            fluctH = sqrt(avgH2 - avgH*avgH);
      
            /* report results */
            if (count%screen == 0)
               printf("%f %d %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f %8.6f\n",
                      count*dt, N, H/N, U/N, U, K/N, K, temp, fluctH/N);
         }
         break;
   }
   free(index);
   fclose(file);
   fclose(file2);
   fclose(kinetic);
   fclose(hamiltonians);
   fclose(shadows);
   fclose(weights);
   if (met == 3 || met == 4)
      printf("The number of accepted tests is %d (%f %%)\n",naccept,(double)naccept/numSteps*100);
   else // HMC
      printf("The number of accepted tests is %d (%f %%)\n",naccept,(double)naccept/(numSteps/iL)*100);
}

/* main program */
int main()
{
   /* read parameters */
   printf("#Decide which method is used (met): ");
   fflush(stdout);
   scanf("%d ", &met);

   printf("#Enter maximum number of particles (M): ");  
   fflush(stdout);
   scanf("%d ", &M);

   printf("#Enter expected average number of particles (N): ");  
   fflush(stdout);
   scanf("%d ", &N);

   printf("#Enter density (rho): ");
   fflush(stdout);
   scanf("%lf ", &rho);

   printf("#Enter chemical potential (mu): ");
   fflush(stdout);
   scanf("%lf ", &mu);

   printf("#Enter initial temperature (T): ");
   fflush(stdout);
   scanf("%lf ", &T);
   beta = 1/T; // compute thermodynamic beta

   if (met == 0 || met == 1 || met == 2 || met == 3) // HMC, GHMC, GSHMC and Yao
   {
      printf("#Enter probability of a movement: ");
      fflush(stdout);
      scanf("%lf ", &pr_mv);
   
      printf("#Enter probability of an insertion: ");
      fflush(stdout);
      scanf("%lf ", &pr_in);
   
      printf("#Enter probability of a deletion: ");
      fflush(stdout);
      scanf("%lf ", &pr_de);
   }
   else // Rowley
   {
      printf("#Enter probability of a movement: ");
      fflush(stdout);
      scanf("%lf ", &pr_mv);
   
      printf("#Enter probability of an insertion/deletion: ");
      fflush(stdout);
      scanf("%lf ", &pr_inde);
   }

   printf("#Enter number of steps: ");  
   fflush(stdout);
   scanf("%li ", &numSteps);

   printf("#Enter time step (dt): "); 
   fflush(stdout);
   scanf("%lf ", &dt);

   printf("#Enter printing frequency: ");
   fflush(stdout);
   scanf("%d ", &screen);

   printf("#Enter writing frequency: ");
   fflush(stdout);
   scanf("%d ", &logfile);

   /* specific for HMC/GHMC/GSHMC */
   if (met == 0 || met == 1 || met == 2)
   {
      printf("#Enter length of trajectory (L): ");
      fflush(stdout);
      scanf("%d ", &iL);
      
      printf("#Enter length of slow-growth (l): ");
      fflush(stdout);
      scanf("%d ", &il);

      int gcd_screen, gcd_log;
      gcd_screen = gcd(iL,screen);
      gcd_log = gcd(iL,logfile);

      if (gcd_screen != iL)
         screen = iL;
      
      if (gcd_log != iL)
         logfile = iL;
   }

   /* specific for GHMC/GSHMC */
   if (met == 1 || met == 2)
   {
      printf("#Enter angle (phi): ");
      fflush(stdout);
      scanf("%lf ", &phi);
   }

   printf("#Enter type of cut-off: ");
   fflush(stdout);
   scanf("%s ", &tcutoff);

   /* initialize random number generator*/
   gsl_rng_env_setup(); //reads the environment variables

   //printf("#Enter random seed: "); 
   //fflush(stdout);
   //scanf("%ld", &seed);
   seed = time(NULL)*getpid();
   gsl_rng_default_seed = seed;
   rng_t = gsl_rng_default;
   g_rng = gsl_rng_alloc(rng_t);
   gsl_rng_set(g_rng, seed);

   /* determine size of cubic box */
   L = cbrt(N/rho); // the volume has to be calculated for the N real particles
   printf("Size of the cubic box = %f\n",L);

   /* report parameters */
   printf("\n#M=%d N=%d L=%lf T=%lf numSteps=%li dt=%lf seed=%ld\n",
          M, N, L, T, numSteps, dt, seed);

   /* open PDB file */
   pdb = fopen("trajLJ.pdb", "wb");
   fprintf(pdb,"COMPND    L = %16.6f\n",L);

   /* run the simulation */
   run(); 

   /* define box and close PDB file */
   fprintf(pdb,"HETATM 9001   C  BOX     1     %7.3f %7.3f %7.3f\n",-L/2,-L/2,-L/2);
   fprintf(pdb,"HETATM 9002   C  BOX     2     %7.3f %7.3f %7.3f\n",-L/2,-L/2,+L/2);
   fprintf(pdb,"HETATM 9003   C  BOX     3     %7.3f %7.3f %7.3f\n",-L/2,+L/2,-L/2);
   fprintf(pdb,"HETATM 9004   C  BOX     4     %7.3f %7.3f %7.3f\n",-L/2,+L/2,+L/2);
   fprintf(pdb,"HETATM 9005   C  BOX     5     %7.3f %7.3f %7.3f\n",+L/2,-L/2,-L/2);
   fprintf(pdb,"HETATM 9006   C  BOX     6     %7.3f %7.3f %7.3f\n",+L/2,-L/2,+L/2);
   fprintf(pdb,"HETATM 9007   C  BOX     7     %7.3f %7.3f %7.3f\n",+L/2,+L/2,-L/2);
   fprintf(pdb,"HETATM 9008   C  BOX     8     %7.3f %7.3f %7.3f\n",+L/2,+L/2,+L/2);
   fprintf(pdb,"CONECT 9001 9002        \n");
   fprintf(pdb,"CONECT 9001 9003        \n");
   fprintf(pdb,"CONECT 9001 9005        \n");
   fprintf(pdb,"CONECT 9002 9004        \n");
   fprintf(pdb,"CONECT 9002 9006        \n");
   fprintf(pdb,"CONECT 9003 9004        \n");
   fprintf(pdb,"CONECT 9003 9007        \n");
   fprintf(pdb,"CONECT 9004 9008        \n");
   fprintf(pdb,"CONECT 9005 9006        \n");
   fprintf(pdb,"CONECT 9005 9007        \n");
   fprintf(pdb,"CONECT 9006 9008        \n");
   fprintf(pdb,"CONECT 9007 9008        \n");
   fclose(pdb);

   gsl_rng_free(g_rng);

   return 0;
}
